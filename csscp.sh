serveradd=hirtle_d@faepnx.tugraz.at
remotedir=Dokumente/master_sem2/cs

if [ "$1" == "pull" ]; then
  scp -r $serveradd:$remotedir/$2 ./
elif [ "$1" == "push" ]; then
  scp -r ./$2 $serveradd:$remotedir/
else
  echo "Unable to parse command!"
fi
