import numpy as np 

def measurements(m): 
    
    # calculate the internal energy 
    energy = - 1.0/m.N * np.matmul(m.S, (m.S[m.neib[:, 1]] + \
                                         m.S[m.neib[:, 2]])) 
    
    # it's easy to get the average from this 
    magnetization = sum(m.S) / m.N 
    
    # return the values 
    return energy, magnetization
