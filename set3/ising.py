from get_neighbor import *
import numpy
import matplotlib.pyplot as mpl
import matplotlib.animation as animation
from statistics import mean, stdev
from matrix_utils import *
from sweep import *
from measurements import *
from ising_graphics import *
from autocorr import *
import time
import os

class Ising:
  def __init__(self, beta_J, L):
    self.beta_J = beta_J;
    self.L = L;
    self.N = L*L;
    self.neib = numpy.array(get_neighbor(L));
    self.reset();
    self._get_probabilities();
    self.E = [];
    self.M = [];
    self.logData = False;

  def _get_probabilities(self):
    dH_index = numpy.array([0, 1, 2, 3, 4]);
    self.transition_probability = \
        numpy.exp(-self.beta_J * 4 * (dH_index - 2));

  def _update_lattice_plot(self, S_ext):
    self.S_ax_handle.set_data(to_matrix(S_ext));
    if self.disco:
      self.E_ax.plot(self.E);
      self.M_ax.plot(self.M);
      self.S_fig.canvas.draw();
      self.S_fig.canvas.flush_events();

  def _run(self):
    for update_it in range(self.num_updates):
      for run in range(self.iterations):
        sweep(self);
        E_, M_ = measurements(self);
        self.E.append(E_);
        self.M.append(M_);
        yield self.S;

  def _run_noyield(self):
    for update_it in range(self.num_updates):
      for run in range(self.iterations):
        sweep(self);
      E_, M_ = measurements(self);
      self.E.append(E_);
      self.M.append(M_);

  def simulate(self, foldername, num_measurements = 1000, num_skips = 0, 
      thermalize = 0):
    skips = int(numpy.ceil(num_measurements / 10000));
    self.file = open(foldername + '/simulation.log', 'w');
    self.resf = open(foldername + '/observables.log', 'w');
    self.fname = foldername;
    self.println("Starting Simulation for " + str(num_measurements) + \
        " measurements");
    self.println("Doing " + str(num_skips + 1) + " sweeps between measurements");
    self.println("Grid-Size: " + str(self.L) + "x" + str(self.L));
    self.E = [];
    self.M = [];
    self.reset();
    sweep(self, thermalize);
    self.iterations = num_skips + 1;
    self.num_updates = num_measurements;
    tic1 = time.time();
    self._run_noyield();
    tic2 = time.time();
    time_run = tic2 - tic1;
    t = thermalize + (num_skips + 1) * numpy.linspace(0, num_measurements * 
        (num_skips + 1) - 1, num_measurements);
    O_fig, (ax_E, ax_M) = mpl.subplots(1, 2, figsize=(12, 6));
    ax_E.plot(t[0:-1:skips], self.E[0:-1:skips]);
    ax_M.plot(t[0:-1:skips], self.M[0:-1:skips]);
    O_fig.suptitle(str(num_measurements) + " measurements, " +
        str(num_skips + 1) + " sweeps between measurements", fontsize = 18);
    ax_E.set_xlabel('runtime / sweeps');
    ax_E.set_ylabel('E / (lattice site)');
    ax_M.set_xlabel('runtime / sweeps');
    ax_M.set_ylabel('M / (lattice site)');
    ax_E.set_title('energy', fontsize = 16);
    ax_M.set_title('magnetization', fontsize = 16);
    O2_fig, (ax2_E, ax2_M) = mpl.subplots(1, 2, figsize=(12, 6));
    ax2_E.plot(t[0:min(100, len(t))], self.E[0:min(100, len(t))]);
    ax2_M.plot(t[0:min(100, len(t))], self.M[0:min(100, len(t))]);
    O2_fig.suptitle(str(num_measurements) + " measurements, " +
        str(num_skips + 1) + " sweeps between measurements", fontsize = 18);
    ax2_E.set_xlabel('runtime / sweeps');
    ax2_E.set_ylabel('E / (lattice site)');
    ax2_M.set_xlabel('runtime / sweeps');
    ax2_M.set_ylabel('M / (lattice site)');
    ax2_E.set_title('energy', fontsize = 16);
    ax2_M.set_title('magnetization', fontsize = 16);
    tic3 = time.time();
    time_plot = tic3 - tic2;
    fig, meanE, stdevE, meanModM, stdevModM = self.analyze();
    time_analyze = time.time() - tic3;
    O_fig.savefig(foldername + "/time_series.png", dpi = 400);
    O2_fig.savefig(foldername + "/time_series_zoomed.png", dpi = 400);
    fig.savefig(foldername + "/autocorrelations.png", dpi = 400);
    np.savetxt(foldername + "/data.txt", np.c_[t, self.E, self.M], \
    delimiter = ', ');
    mpl.close(fig);
    mpl.close(O_fig);
    mpl.close(O2_fig);
    self.println("Simulation took " + str(time_run) + " sec");
    self.println("Plotting took   " + str(time_plot) + " sec");
    self.println("Analyzing took  " + str(time_analyze) + " sec");
    self.file.close();
    self.resf.close();
    return meanE, stdevE, meanModM, stdevModM;

  def animated_run(self, sweeps = 1000, num_updates = -1, disco=False):
    self.reset();
    if num_updates == -1: num_updates = sweeps;
    self.iterations = int(sweeps / num_updates);
    self.num_updates = num_updates;
    self.disco = disco;
    if disco:
      self.S_fig, ((self.S_ax,_), (self.E_ax, self.M_ax)) = \
          mpl.subplots(2, 2, figsize=(12,8));
      self.E_ax_handle, = self.E_ax.plot([]);
      self.S_fig.suptitle("1 sweep/frame, animated run", fontsize = 20);
      self.S_ax.set_title("lattice", fontsize = 16);
      self.E_ax.set_title("energy", fontsize = 16);
      self.E_ax.set_ylabel("E / (lattice site)");
      self.E_ax.set_xlabel("number of runs");
      self.M_ax.set_title("magentization", fontsize = 16);
      self.M_ax.set_ylabel("M / (lattice site)");
      self.M_ax.set_xlabel("number of runs");
    else:
      self.S_fig, self.S_ax = mpl.subplots(figsize=(6,6));
    self.S_ax_handle = self.S_ax.matshow(to_matrix(self.S));
    ani = animation.FuncAnimation(self.S_fig, self._update_lattice_plot,
        self._run, interval=1, repeat=False);
    mpl.show();
    self.analyze();

  def analyze(self):
    t_max = 25;
    tau_E, tau_M, fig = evaluateCorrelations(self, t_max);
    mean_E = mean(self.E);
    stdev_E = stdev(self.E) * numpy.sqrt(2 / len(self.E) * tau_E);
    mean_M = mean(self.M);
    stdev_M = stdev(self.M) * numpy.sqrt(2 / len(self.M) * tau_M);
    mod_M = np.absolute(self.M);
    mean_mod_M = mean(mod_M);
    t = np.linspace(0, t_max - 1, t_max);
    _, _, tau_mod_M = calcCorrelation(mod_M, t_max, t);
    stdev_mod_M = stdev(mod_M) * numpy.sqrt(2 / len(mod_M) * tau_mod_M);
    self.println("E = " + str(mean_E) + " +- " + str(stdev_E));
    self.println("M = " + str(mean_M) + " +- " + str(stdev_M));
    self.println("|M| = " + str(mean_mod_M) + " +- " + str(stdev_mod_M));
    self.resf.write("E = " + str(mean_E) + " +- " + str(stdev_E) + '\n');
    self.resf.write("M = " + str(mean_M) + " +- " + str(stdev_M) + '\n');
    self.resf.write("|M| = " + str(mean_mod_M) + " +- " + str(stdev_mod_M)
         + '\n');
    return fig, mean_E, stdev_E, mean_mod_M, stdev_mod_M
  
  def println(self, prntstr):
    if self.logData:
      self.file.write(prntstr + '\n');
      print('<' + os.sep.join(self.fname.split(os.sep)[1:]) + '> ' + prntstr);
    else:
      print(prntstr);
  
  def reset(self):
    self.S = numpy.random.choice([-1, 1], size=self.L*self.L);
