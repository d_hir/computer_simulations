import numpy
from sweep import *
from fancyprint import *
from ising_graphics import *
from ising import *
from measurements import *
from statistics import mean
import matplotlib.pyplot as mpl
import os

VALS52 = 20;
NMEAS = 1000000;

# execution funciton has to be in main code
################################################################################
import multiprocessing
from directories import makedirs

def doSimulation(L, beta, nequi, fname, nmeas = NMEAS, nskip = 0):
  m = Ising(beta, L);
  m.logData = True;
  res = m.simulate(fname, nmeas, nskip, nequi);
  del m;
  return res;

def execute(mode = 'parallel'):
  if __name__ == '__main__':
    beta_C = 0.44068;
    therm = 10;
    arg41 = [[4, 0.2, 0],     [4, 0.4, 0],     [4, 0.6, 0],
             [6, 0.2, therm], [6, 0.4, therm], [6, 0.6, therm],
             [8, 0.2, therm], [8, 0.4, therm], [8, 0.6, therm]];
    arg42 = [[4,  beta_C, therm], [8,  beta_C, therm],
             [16, beta_C, therm], [32, beta_C, therm]];
    arg51 = [[8,  0.2, therm], [8,  beta_C, therm], [8,  0.6, therm],
             [32, 0.2, therm], [32, beta_C, therm], [32, 0.6, therm]];
    L = [4, 8, 16, 32];
    beta = [(0.8 / VALS52) * n for n in range(1, VALS52)];
    arg52 = [[l, b, therm] for l in L for b in beta];
    args = arg41 + arg42 + arg51 + arg52;
    foldernames = makedirs([arg41, arg42, arg51, arg52]);
    argtot = [a + [f] for a, f in zip(args, foldernames)];
    
    tic = time.time();
    
    if mode == 'parallel':
      p = multiprocessing.Pool(multiprocessing.cpu_count());
      res = p.starmap(doSimulation, argtot);
      p.close();
    else:
      res = [];
      for a in argtot:
        res.append(doSimulation(*a));
        
    elapsed = time.time() - tic;
    print("Total Simulation took " + str(elapsed) + " sec");
    
    resi = numpy.array([a + list(r) for a, r in zip(arg52, res[-len(arg52):])]);
    np.savetxt(os.sep.join(foldernames[-1].split(os.sep)[:2]) + "/data.txt", 
        resi, delimiter = ', ');
    
    fig, ax = mpl.subplots(2, len(L), figsize=(24, 18));
    for l in range(len(L)):
      ax[0][l].errorbar(resi[(l*len(beta)):((l+1)*len(beta)), 1], 
          resi[(l*len(beta)):((l+1)*len(beta)), 3], 
          resi[(l*len(beta)):((l+1)*len(beta)), 4], 
          linestyle = '', marker = '+', mec = 'r');
      ax[1][l].errorbar(resi[(l*len(beta)):((l+1)*len(beta)), 1], 
          resi[(l*len(beta)):((l+1)*len(beta)), 5], 
          resi[(l*len(beta)):((l+1)*len(beta)), 6], 
          linestyle = '', marker = '+', mec = 'r');
      ax[0][l].set_xlabel('beta * J / 1');
      ax[1][l].set_xlabel('beta * J / 1');
      ax[0][l].set_ylabel('E / (lattice site)');
      ax[1][l].set_ylabel('M / (lattice site)');
      ax[0][l].set_title('energy: L = ' + str(L[l]));
      ax[1][l].set_title('magnetization: L = ' + str(L[l]));
    fig.suptitle('Energy E, Magnetization M for various L', fontsize = 32);
    fig.savefig(os.sep.join(foldernames[-1].split(os.sep)[:2]) + "/result.png", dpi = 400);
    mpl.close(fig);
      

################################################################################

m = Ising(0.1, 8);
#os.makedirs('2x2_0.4_1e7');
#m.simulate("2x2_0.4_1e7", 10000000, 4, 10000);

m.animated_run(100, -1, True);

#execute('parallel');

