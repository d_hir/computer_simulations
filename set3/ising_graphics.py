import matplotlib.pyplot as mpl
from matplotlib.widgets import Slider
from matrix_utils import *


def create_slider(S_list_lin, x_vals = None, x_name = "J") :
  if x_vals == None:
    x_vals = list(range(len(S_list_lin)));
  S_list = [to_matrix(S_lin) for S_lin in S_list_lin];
  fig = mpl.figure(figsize=(5, 6));
  ax = mpl.subplot(111);
  fig.subplots_adjust(bottom=0.25);
  img = ax.matshow(S_list[0]);
  ax = fig.add_axes([0.15, 0.1, 0.65, 0.03])
  dJ = x_vals[1] - x_vals[0];
  slider = Slider(ax, x_name, x_vals[0], x_vals[-1], valinit=x_vals[0], 
      valfmt='%.2f', valstep=dJ)
  def update(val):
    wantedj = float(slider.val);
    indexj = int((wantedj - x_vals[0]) / dJ);
    img.set_data(S_list[indexj]);
    fig.canvas.draw();
  slider.on_changed(update);

def plot(O):
  mpl.figure(figsize=(5, 6));
  mpl.plot(O);

def show_plots():
  mpl.show();
  
def sliderplot() :
  L = 8;

  m = Ising(2, L);

  S_list = [numpy.copy(m.S)];

  for j in range(100):
    sweep(m);
    S_list.append(numpy.copy(m.S));

  create_slider(S_list, x_name = "t");
  show_plots();
