def get_neighbor(L): 

    if not type(L) == int or L < 2: 
        print("get_neighbor: Input parameter L does not make sense.") 
        return 
    
    neighbor = [] 
    
    for site in range(L*L) :
        
        if (site+1)%L :
            right = site + 2 
        else :
            right = site - L + 2 
            
        if (site)%L :
            left = site  
        else : 
            left = site + L 
    
        up = site + L + 1
        if up > L*L : 
            up = up - L*L 
        
        down = site - L + 1
        if down < 1 :
            down = down + L*L
            
        neighbor.append([up -1, right -1, down -1, left -1]) 
    
    return neighbor
    
