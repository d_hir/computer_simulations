import numpy
import random

def sweep(m, num_sweeps=1):
  random_sites = numpy.random.randint(m.N, size=m.N * num_sweeps);
  rands = numpy.random.random(m.N * num_sweeps);
  for random_site, r in zip(random_sites, rands):
    dH = int(0);
    for nu in range(4):
      dH += m.S[m.neib[random_site, nu]];
    dH *= m.S[random_site];
    dH = int(dH / 2 + 2);
    if r < m.transition_probability[dH]:
      m.S[random_site] = - m.S[random_site];
