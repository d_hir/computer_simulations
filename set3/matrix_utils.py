import numpy

def to_matrix(v):
  numel = len(v);
  L = int(numpy.sqrt(numel));
  return [v[k*L:(k+1)*L] for k in range(L)];
