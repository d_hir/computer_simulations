import time
from ising import *
from sweep import *
import matplotlib.pyplot as plt
import numpy


def doTiming() :
  timelist = [];

  max_L_exponent = 10;

  L_list = numpy.logspace(1, max_L_exponent, max_L_exponent, base=2)

  for L in L_list:
    m = Ising(0.4, int(L));
    tic = time.time();
    sweep(m);
    timelist.append(time.time() - tic);

  print(timelist);

  N = L_list ** 2;
  
  p = numpy.polyfit(N, timelist, 1);
  ttheor = numpy.linspace(N[0], N[-1], 100);
  timefit = numpy.polyval(p, ttheor);
  
  fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(17, 8));
  fig.suptitle('Time for 1 sweep in LxL lattice');
  ax1.plot(N, timelist, 'o');
  ax1.plot(ttheor, timefit, ':');
  ax1.set_xlabel('L / 1');
  ax1.set_ylabel('time / s');
  ax1.legend(['time measurement', 'linear fit']);
  ax2.semilogx(N, timelist, 'o');
  ax2.semilogx(ttheor, timefit, ':');
  ax2.set_xlabel('L / 1');
  ax2.set_ylabel('time / s');
  ax2.legend(['time measurement', 'linear fit']);
  plt.show();

doTiming();
  
L = 8;
m = Ising(0.4, L);
N = 1000;
tic = time.time();
for j in range(N):
  sweep(m);
dt = time.time() - tic;
print("Time needed for " + str(N) + " sweeps: {:.3f} sec".format(dt));
print("Estimated runtime for 1M sweeps: {:.1f} sec".format(1e6 / N * dt));
