#ifndef PARTICLE_H
#define PARTICLE_H

#include <iostream>
#include <cmath>
#include "pvector.h"
#include "rg.h"
#include <cassert>

class Particle {
  private:
    double m = 1.0;
    double kBT = 0.001;
    double box_length;
      
  public:
    PVector<> pos;
    PVector<> vel;
    int dims;
    static double POSITION_STDEV;
    enum class dirs {negative, positive, both};
    static RandomGenerator rg;
    static double RELATIVE_DISTANCE_FROM_WALL;
    int ID;
    
    Particle(int dims_ = 2) : pos{dims_}, vel{dims_}, dims{dims_} {
      this->ID = rg.randi();
      this->pos.randomize();
    };
    
    Particle(int dims_, int index, double spacing, double box_length_) : 
        box_length{box_length_}, pos{dims_}, vel{dims_}, dims{dims_} {
      this->ID = rg.randi();
      int L = floor(this->box_length / spacing);
      std::vector<double> p;
      std::vector<int> indices (this->dims);
      indices[this->dims - 1] = floor(index / pow(L, this->dims - 1));
      for (int d = this->dims - 2; d >= 0; d--) {
        indices[d] = floor((index % (int) pow(L, d + 1)) / pow(L, d));
      }
      double margin = (this->box_length - (L - 1) * spacing) / 2;
      for (auto& j : indices)
        p.push_back(margin + j * spacing);
      pos.set(p);
      pos.perturbate(Particle::POSITION_STDEV);
      vel.perturbate(this->kBT / this->m);
    };
    
    void checkReflection(int axis = 0, dirs dir = dirs::both) {
      assert(this->dims > axis);
      if ((dir == dirs::both || dir == dirs::negative) && 
          this->pos.v[axis] < 0) {
        //std::cout << "Reflected negative: " << this->vel.v[axis];
        this->vel.v[axis] = -this->vel.v[axis];
        this->pos.v[axis] = Particle::RELATIVE_DISTANCE_FROM_WALL;
        //std::cout << " -> " << this->vel.v[axis] << std::endl;
      }
      if ((dir == dirs::both || dir == dirs::positive) && 
          this->pos.v[axis] > this->box_length) {
        //std::cout << "Reflected positive: " << this->vel.v[axis];
        this->vel.v[axis] = -this->vel.v[axis];
        this->pos.v[axis] = this->box_length - 
            Particle::RELATIVE_DISTANCE_FROM_WALL;
        //std::cout << " -> " << this->vel.v[axis] << std::endl;
      }
    };
    
    void setTemperature(double kBT_) {
      this->kBT = kBT_;
      this->vel.set(std::vector<double> (this->dims, 0.0));
      this->vel.perturbate(this->kBT / this->m);
    };
    
    void print() {
      std::cout << " x = ";
      this->pos.print(false);
      std::cout << " v = ";
      this->vel.print();
    };
    
    double mass() {
      return this->m;
    };
    
    double vel_squared() {
      return this->vel.inner_product(this->vel);
    };
};

double Particle::POSITION_STDEV = 0.03;
double Particle::RELATIVE_DISTANCE_FROM_WALL = 1e-3;

RandomGenerator Particle::rg = RandomGenerator(0, int(1e16), 0.0, 1.0);

bool operator==(const Particle& p1, const Particle& p2) {
  return p1.ID == p2.ID;
};

#endif
