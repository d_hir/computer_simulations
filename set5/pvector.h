#ifndef PVECTOR_H
#define PVECTOR_H

#include <vector>
#include <iostream>
#include <cassert>
#include "rg.h"

enum class Metric {
    euclidean=1, standard=1,
    manhattan=2, taxicab=2,
    minkowski=3, spacetime=3
};

template<typename T = double>
class PVector {
  private:
    double stdev_perturbation = 1.0;
    
  public:
    static RandomGenerator rg;
    std::vector<T> v;
    int dims;
    
    PVector(int dims_, T val = T(0)) : v{std::vector<T>(dims_, val)}, 
        dims{dims_} {
      rg.setupRandn(0.0, stdev_perturbation);
    }
    
    void set(std::vector<T> v2) {
      this->v = v2;
    }
    
    void randomize() {
      std::cout << "Placed Molecule @ (";
      for (auto& vi : v) {
        vi = rg.rand();
        std::cout << vi;
        if (vi == v.back())
          std::cout << ")" << std::endl;
        else
          std::cout << ", ";
      }
    }
    
    void perturbate(double stdev) {
      if (std::abs(this->stdev_perturbation - stdev) > 1e-3) {
        rg.setupRandn(0.0, stdev);
        this->stdev_perturbation = stdev;
      }
      for (auto& vi : v)
        vi = vi + rg.randn();
    }
    
    T x() {
      if (dims >= 1)
        return v[0];
      return T(0);
    }
    
    T y() {
      if (dims >= 2)
        return v[1];
      return T(0);
    }
    
    static T distance(PVector& T1, PVector& T2, Metric m = Metric::euclidean) {
      T dist = T(0.0);
      switch (m) {
        case Metric::euclidean: // standard euclidean metric
          for (int j = 0; j < T1.dims; j++)
            dist += (T1.v[j] - T2.v[j]) * (T1.v[j] - T2.v[j]);
          dist = std::sqrt(dist);
          break;
        case Metric::manhattan: // manhattan/taxicab metric
          for (int j = 0; j < T1.dims; j++)
            dist += std::abs(T1.v[j] - T2.v[j]);
          break;
        case Metric::minkowski: // squared minkowski metric (+---)
          dist += (T1.v[0] - T2.v[0]) * (T1.v[0] - T2.v[0]);
          for (int j = 1; j < T1.dims; j++)
            dist -= (T1.v[j] - T2.v[j]) * (T1.v[j] - T2.v[j]);
          break;
        default:
          std::cout << "TOWN: Metric undefined: " << (int) m << std::endl;
          dist = T(-1.0);
      }
      return dist;
    };
    
    void subtract(const PVector& P) {
      assert(this->dims == P.dims);
      for (int j = 0; j < this->dims; j++) {
        this->v[j] -= P.v[j];
      }
    };
    
    void add(const PVector& P) {
      assert(this->dims == P.dims);
      for (int j = 0; j < this->dims; j++) {
        this->v[j] += P.v[j];
      }
    };
    
    template<typename U>
    void operator*=(U s) {
      for (auto& vi : this->v)
        vi *= s;
    };
    
    template<typename U>
    PVector operator*(U s) {
      PVector p = *this;
      p *= s;
      return p;
    };
    
    void operator+=(const PVector& p) {
      assert(this->dims == p.dims);
      for (int j = 0; j < this->dims; j++)
        this->v[j] += p.v[j];
    };
    
    PVector operator+(const PVector& p) {
      PVector ret = *this;
      ret += p;
      return ret;
    };
    
    void absolutify() {
      for (auto& vi : this->v)
        vi = std::abs(vi);
    };
    
    void print(bool end_with_newline = true) {
      std::cout << "[";
      for (int j = 0; j < this->dims - 1; j++)
        std::cout << this->v[j] << ", ";
      std::cout << *(this->v.end() - 1) << "]";
      if (end_with_newline)
        std::cout << std::endl;
    };
    
    T inner_product(const PVector& p) {
      assert(this->dims == p.dims);
      T res = T(0);
      for(int j = 0; j < this->dims; j++)
        res += p.v[j] * this->v[j];
      return res;
    };
};

template<typename T>
RandomGenerator PVector<T>::rg = RandomGenerator(0, 1, 0.0, 1.0);

#endif
