import scipy.stats
import matplotlib.pyplot as mpl
import csv
import numpy

BINS = 50;

with open('velocities.csv', 'r') as csvfile:
  reader = csv.reader(csvfile, delimiter='\n');
  velocities = [float(i[0]) for i in list(reader)];

v_theor = numpy.linspace(min(velocities), max(velocities), 100);
params = scipy.stats.maxwell.fit(velocities, floc = 0);
fv_theor = scipy.stats.maxwell.pdf(v_theor, *params);

print("Maxwell fit parameter: " + str(params[1]));

fig_v, ax_v = mpl.subplots();
ax_v.hist(velocities, bins=BINS, density = True);
ax_v.plot(v_theor, fv_theor, '--');
ax_v.set_xlabel(r'|v| / $\frac{\epsilon}{m}$');
ax_v.set_ylabel('P(|v|)');
ax_v.set_title('Histogram of absolute velocity |v|');
fig_v.tight_layout();

with open('heights.csv', 'r') as csvfile:
  reader = csv.reader(csvfile, delimiter='\n');
  heights = [float(i[0]) for i in list(reader)];

fig_h, ax_h = mpl.subplots();
ax_h.hist(heights, bins=BINS, density = True);
ax_h.set_xlabel(r'h / $\sigma$');
ax_h.set_ylabel('P(h)');
ax_h.set_title('Height distribution histogram');
fig_h.tight_layout();

mean_vel_sqr = numpy.mean([j * j for j in velocities]) / 2;
print("Mean of squared velocity: " + str(mean_vel_sqr));

hist_heights, bin_edges = numpy.histogram(heights, bins = BINS, density = True);
p = numpy.polyfit(bin_edges[:-1], numpy.log(hist_heights), 1);
hs = - 1.0 / p[0];
rho_0 = numpy.exp(p[1]);
print("Barometric height fit: " + str(hs) + " * g");
h_theor = numpy.linspace(min(heights), max(heights), 100);
ax_h.plot(h_theor, rho_0 * numpy.exp(- h_theor / hs));

mpl.show();
