#include <iostream>
#include <cmath>
#include <vector>
#include "rg.h"
#include "histogram.h"

#define PI 3.14159265358979323
#define NUM_STATISTICS 1000
#define NUM_BARS 25
#define MAX_REJECTIONS_PER_TRY 1000

#define CAUCHY_S 1.0
#define ALTERNATE_S 0.37

#define DO_SIMPLE_EVALS false
#define DO_CAUCHY true
#define DO_REJECTION_RHO true
#define DO_REJECTION_ALT true
#define DO_ANALYSIS_COMPARISON true
#define DO_INTEGRAL_COMPARISON true

RandomGenerator generator(0, 1, 0.0, 1.0);

int cauchy_rejected = 0;
int alternate_rejected = 0;

double getCauchyRandom(double s) {
  double uniform_random = generator.rand();
  return s * tan(PI * (uniform_random - 0.5));  
  //G = 1/pi arctan(x/s) + 1/2
  //(G -1/2)*pi = arctan(x/s)
  //s * tan(pi * (G - 1/2)) = x
}

double getAlternateRandom(double s, double x0) {
  double xi = generator.rand();
  double c = 2.0 * (s * x0 + 1.0 / x0);
  double xi0 = 1.0 / (c * x0);
  double xi1 = 1.0 / (c * x0) + 2.0 * s * x0 / c;
  if (xi < xi0)
    return -1.0 / (c * xi);
  else if (xi < xi1)
    return (c * xi / s - 1 / (s * x0) - x0);
  else
    return 1.0 / (2.0 / x0 + 2 * s * x0 - c * xi);
}

double getRejectionRho() {
  double xt;
  int j = 0;
  double r;
  for (; j < MAX_REJECTIONS_PER_TRY; j++) {
    xt = getCauchyRandom(1.0);
    r = generator.rand();
    if (r < sin(xt) * sin(xt))
      break;
    cauchy_rejected++;
  }
  if (j == MAX_REJECTIONS_PER_TRY) {
    std::cerr << "ERROR: MAX REJECTIONS REACHED" << std::endl;
    return nan("");
  }
  return xt;
}

double hAlternate(double x, double s, double x0) {
  if (abs(x) < x0)
    return s;
  else 
    return 1.0 / (x * x);
}

double hAlternateGen(double x) {
  return hAlternate(x, ALTERNATE_S, sqrt(1 / ALTERNATE_S));
}

double gRho(double x) {
  return sin(x)*sin(x) / (1 + x*x);
}

double hCauchy(double x, double s) {
  return s / (s*s + x*x);
}

double hCauchyGen(double x) {
  return hCauchy(x, CAUCHY_S);
}

double cauchyRandomGenerator() {
  return getCauchyRandom(CAUCHY_S);
}

double getRejectionAlternate() {
  double xt;
  double r;
  double s = ALTERNATE_S;
  int j = 0;
  for (; j < MAX_REJECTIONS_PER_TRY; j++) {
    xt = getAlternateRandom(s, sqrt(1 / s));
    r = generator.rand();
    if (r * hAlternate(xt, s, sqrt(1 / s)) < gRho(xt))
      break;
    alternate_rejected++;
  }
  if (j == MAX_REJECTIONS_PER_TRY) {
    std::cerr << "ERROR: MAX REJECTIONS REACHED" << std::endl;
    return nan("");
  }
  return xt;
}

double getIntegralDiff(double f1(double), double f2(double)) {
  double range = 50;
  double num_vals = 10000;
  std::vector<double> f1vals;
  std::vector<double> f2vals;
  for (double x = -0.5 * range; x < 0.5 * range; x += range / num_vals) {
    f1vals.push_back(f1(x));
    f2vals.push_back(f2(x));
  }
  double intf1 = 0;
  for (auto& elemf1 : f1vals)
    intf1 += elemf1;
  double intf2 = 0;
  for (auto& elemf2 : f2vals)
    intf2 += elemf2;
  return intf1 / intf2;
}


int main() {
  if (DO_SIMPLE_EVALS) {
    double cauchy = getCauchyRandom(1.0);
    std::cout << cauchy << std::endl;
  
    double rho = getRejectionRho();
    std::cout << rho << std::endl;
  }
  
  if (DO_CAUCHY) {
    std::vector<double> cauchy_vec (NUM_STATISTICS);
    std::generate(cauchy_vec.begin(), cauchy_vec.end(), cauchyRandomGenerator);
    Histogram hist_cauchy (NUM_BARS, -5.0, 5.0);
    hist_cauchy.setData(cauchy_vec);
    hist_cauchy.plot(2);
    hist_cauchy.plot(3);
  }
  
  if (DO_REJECTION_RHO) {
    std::vector<double> rejrho_vec (NUM_STATISTICS);
    std::generate(rejrho_vec.begin(), rejrho_vec.end(), getRejectionRho);
    Histogram hist_rejrho (NUM_BARS, -5.0, 5.0);
    hist_rejrho.setData(rejrho_vec);
    hist_rejrho.plot(2);
    hist_rejrho.plot(3);
    double cauchy_rejection_rate = (double) cauchy_rejected /
        (cauchy_rejected + NUM_STATISTICS);
    std::cout << "Cauchy rejection rate:    " << cauchy_rejection_rate 
        << std::endl;
  }
  
  if (DO_REJECTION_ALT) {
    std::vector<double> rejalt_vec (NUM_STATISTICS);
    std::generate(rejalt_vec.begin(), rejalt_vec.end(), getRejectionAlternate);
    Histogram hist_rejalt (NUM_BARS, -5.0, 5.0);
    hist_rejalt.setData(rejalt_vec);
    hist_rejalt.plot(2);
    hist_rejalt.plot(3);
    double alternate_rejection_rate = (double) alternate_rejected /
        (alternate_rejected + NUM_STATISTICS);
    std::cout << "Alternate rejection rate: " << alternate_rejection_rate
        << std::endl;
  }

  if (DO_ANALYSIS_COMPARISON) {
    std::vector<double> diff_vec (10);
    std::generate(diff_vec.begin(), diff_vec.end(), cauchyRandomGenerator);
    Histogram hist_diff (10, -5.0, 5.0);
    hist_diff.setData(diff_vec);
    hist_diff.plot(2);
    hist_diff.plot(3);
  }
  
  if (DO_INTEGRAL_COMPARISON) {
    double diffCauchy = getIntegralDiff(gRho, hAlternateGen);
    std::cout << "Integration Rejection Rate Cauchy:    " << diffCauchy
        << std::endl;
    double diffAlternate = getIntegralDiff(gRho, hCauchyGen);
    std::cout << "Integration Rejection Rate Alternate: " << diffAlternate
        << std::endl;
  }
  return 0;
}
