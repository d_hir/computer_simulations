#ifndef VECTORUTILS_H
#define VECTORUTILS_H

#include <vector>
#include <cmath>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <assert.h>
#include <memory>
#include <stdexcept>
#include <fstream>
#include <string>
#include <numeric>

namespace vectorutils {

template<typename T>
void print(const std::vector<T> &x) {
  std::cout << "--------------" << std::endl;
  for (auto& e : x)
    std::cout << e << std::endl;
}

template<typename It>
void print(It begin_it, It end_it) {
  std::cout << "--------------" << std::endl;
  for (It loop_it = begin_it; loop_it <= end_it; loop_it++)
    std::cout << *loop_it << std::endl;
}

template<typename T, typename U>
void print(std::vector<T> &x1, std::vector<U> &x2) {
  if (x1.size() != x2.size()) {
    std::cout << " VECTORS NOT MATCHING IN SIZE!\n" <<
        " PRINTING ABORTED!" << std::endl;
    return;
  }
  std::cout << "--------------" << std::endl;
  for (int j = 0; j < static_cast<int>(x1.size()); j++)
    std::cout << x1[j] << "   |   " << x2[j] << std::endl;
}

template<typename T>
T mean(const std::vector<T> &x) {
  return std::accumulate(x.begin(), x.end(), 0.0) / x.size();
}

template<typename Iter>
typename std::iterator_traits<Iter>::value_type mean(const Iter begin_it,
    const Iter end_it) {
  return std::accumulate(begin_it, end_it, 0.0) / 
      std::distance(begin_it, end_it);
}

template<typename T>
T var(const std::vector<T> &x) {
  T m = mean(x);
  return std::inner_product(x.begin(), x.end(), x.begin(), 0.0,
      std::plus<>(), 
      [m](T const &x, T const &y){ return (x - m) * (y - m); }) 
      / (x.size() - 1);
}

template<typename T>
void colon(std::vector<T>& vec, T from, T diff = 1) {
  vec[0] = from;
  typename std::vector<T>::iterator it;
  for (it = vec.begin() + 1; it < vec.end(); it++)
    *it = *(it - 1) + diff;
}

template<typename T>
void linspace(std::vector<T>& vec, T from, T to) {
  T diff = (to - from) / (static_cast<int>(vec.size()) - 1);
  colon<T>(vec, from, diff);
}

template<typename T>
void logspace(std::vector<T>& vec, T from, T to, T base = 10) {
  if (to <= from)
    return;
  vec[0] = pow(base, from);
  typename std::vector<T>::iterator it;
  for (it = vec.begin() + 1; it < vec.end(); it++)
    *it = *(it - 1) * base;
}

template<typename T>
int sgn(T in, T midpoint = 0) {
  if (in > midpoint)
    return 1;
  else if (in < midpoint)
    return -1;
  else
    return 0;
}

template<typename T>
std::vector<std::vector<T>> load_csv(std::string fname, char sep = ',') {
  std::vector<std::vector<T>> town_positions;
  std::ifstream data_file (fname);
  if (!data_file.is_open()) throw std::runtime_error("Unable to open file!");
  std::string line;
  T val;
  while (std::getline(data_file, line)) {
    std::stringstream ss (line);
    std::vector<T> tmp_town_coord;
    while (ss >> val) {
      tmp_town_coord.push_back(val);
      if (ss.peek() == sep) ss.ignore();
    }
    town_positions.push_back(tmp_town_coord);
  }
  data_file.close();
  return town_positions;
}

// ########################################################################## //

using namespace boost::numeric::ublas;

///////
// POLYNOMIAL STUFF
//
// ENTRIES OF POLYNOMIAL VECTORS ARE IN RISING POWER!
// p = (a0, a1, ..., aN) ==> p(x) = a0 + a1*x + ... + aN*x^N
///////

template<typename T>
std::vector<T> polyfit(const std::vector<T>& x, const std::vector<T>& y, 
    int deg) {
  assert(x.size() == y.size());
  
  int i_size = x.size();
  int d_size = deg + 1;
  
  matrix<T> X (i_size, d_size);
  matrix<T> Y (i_size, 1);
  
  for (int i = 0; i < i_size; i++) {
    Y(i, 0) = y[i];
    X(i, 0) = 1.0;
    for (int jp = 1; jp < d_size; jp++)
      X(i, jp) = X(i, jp - 1) * x[i];
  }
  
  matrix<T> Xp (trans(X));
  matrix<T> A (prec_prod(Xp, X));
  matrix<T> b (prec_prod(Xp, Y));
  std::vector<T> a (b.data().begin(), b.data().end());
  permutation_matrix<int> perm_matr (A.size1());
  const int ret_factorization = lu_factorize(A, perm_matr);
  assert(ret_factorization == 0);
  lu_substitute(A, perm_matr, b);
  
  return std::vector<T>(b.data().begin(), b.data().end());  
}

template<typename T>
T polyval(const std::vector<T>& p, const T x) {
  T expx = 1;
  T result = 0;
  for (auto& pi : p) {
    result += pi * expx;
    expx *= x;
  }
  return result;
}

template<typename T>
void polyval(std::vector<T>& res, const std::vector<T>& p, const std::vector<T>& x) {
  for (auto& xval : x)
    res.push_back(polyval<T>(p, xval));
}

} // end namespace

#endif
