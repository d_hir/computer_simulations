#ifndef PLOTUTILS_H
#define PLOTUTILS_H

#include <iostream>
#include <vector>
#include <cmath>
#include <boost/tuple/tuple.hpp>
#include <string>
#include <boost/format.hpp>
#include "gnuplot-iostream.h"
#include <memory>

namespace plotutils {

template<typename T = double>
class Figure {
  private:
    Gnuplot gp;
    bool first_plot;
    bool show_key;
    bool is_valid;
    
    template<typename V>
    void _raw_plot(V& x, std::string& data_title, std::string& logmode, 
        std::string& witharg) {
      if (!this->first_plot)
        gp << "re";
      gp << "plot" << gp.file1d(x);
      if (data_title != "") {
        gp << " title \"" << data_title << "\"";
        this->show_key = true;
      }
      if (witharg != "")
        gp << " with " << witharg;
      gp << "\n";
      if (this->show_key)
        gp << "set key on\n";
      this->first_plot = false;
    }
  
  public:    
    Figure(bool validate = true) : first_plot(true), show_key(false), 
        is_valid(validate) {
      gp << "set key off\n";
    }
    
    bool isValid() {
      return this->is_valid;
    }
    
    void plot(std::vector<T> &x, std::string data_title = "",
        std::string logmode = "", std::string witharg = "") {
      _raw_plot(x, data_title, logmode, witharg);
    }

    template<typename U>
    void plot(std::vector<T> &x, std::vector<U> &y, std::string data_title = "",
        std::string logmode = "", std::string witharg = "") {
      auto tpl = boost::make_tuple(x, y);
      _raw_plot(tpl, data_title, logmode, witharg);
    }

    void _set(std::string key, std::string value,
        std::string delim_left = "\"", std::string delim_right = "\"") {
      gp << "set " << key << " " << delim_left << value << delim_right << "\n";
      gp << "replot\n";
    }

    void title(std::string title_str) {
      _set("title", title_str);
    }

    void xlabel(std::string label_str) {
      _set("xlabel", label_str);
    }

    void ylabel(std::string label_str) {
      _set("ylabel", label_str);
    }

    void raw_cmd(std::string cmd, bool replot = true) {
      gp << cmd;
      if (cmd.back() != '\n')
        gp << '\n';
      if (replot)
        gp << "replot\n";
    }
    
    void overrideWithNextPlot() {
      this->first_plot = true;
    }
    
    void flush() {
      gp.do_flush();
    }
};

// ############################ HISTOGRAM CLASS ############################ //

#define PU_TOLERANCE 0.01

class Histogram {
  private:
    int num_bins;
    std::vector<double> bin_width;
    std::vector<int> bin;
    double min;
    double max;
    int remainder_bin;
    Gnuplot gp;
    double range;
    std::vector<double> maxvals;
    int total_elements;
    
    void setMaxVals();
    std::vector<double> frequentistUncertainty();
    std::vector<double> bayesianUncertainty(std::vector<double> avg_pi);
    std::vector<std::string> getXTicks(int num_ticks);
    
  public:
    Histogram(int num_bins_, double min_, double max_);
    int setIndividualWidth(std::vector<double> bin_width_);
    void setData(std::vector<double> data);
    void plot(int variant);
    void plot(int variant, std::string lineplotstring);
    void printBins();
    void _set(std::string key, std::string value,
        std::string delim_left, std::string delim_right);
    void xlabel(std::string label_str);
    void ylabel(std::string label_str);
    void title(std::string title_str);
};

Histogram::Histogram(int num_bins_, double min_, double max_) {
  this->num_bins = num_bins_;
  this->min = min_;
  this->max = max_;
  this->range = this->max - this->min;
  this->bin_width = *(new 
      std::vector<double>(this->num_bins, this->range / this->num_bins));
  this->bin = *(new std::vector<int> (this->num_bins, 0));
  this->total_elements = 0;
  this->remainder_bin = 0;
};

int Histogram::setIndividualWidth(std::vector<double> bin_width_) {
  double sum = 0;
  for (int j = 0; j < this->num_bins; j++)
    sum += bin_width_[j];
  if (sum > this->range * (1 + PU_TOLERANCE) or 
      sum < this->range * (1 - PU_TOLERANCE)) {
    std::cerr << "HISTOGRAM: BIN-WIDTH DOES NOT MATCH PREVIOUS SETTINGS!"
        << std::endl;
    return 1;
  } else {
    this->bin_width = bin_width_;
    return 0;
  }
};

void Histogram::setMaxVals() {
  this->maxvals = *(new std::vector<double> (this->num_bins));
  this->maxvals[0] = this->min + this->bin_width[0];
  for (int j = 1; j < this->num_bins; j++)
    this->maxvals[j] = this->maxvals[j - 1] + this->bin_width[j];
};

void Histogram::setData(std::vector<double> data) {
  Histogram::setMaxVals();
  for (auto d : data) {
    this->total_elements++;
    if (d < this->min || d > this->max) {
      this->remainder_bin++;
      continue;
    }
    for (int j = 0; j < this->num_bins; j++) {
      if (d < maxvals[j]) {
        this->bin[j]++;
        break;
      }
    }  
  }
};

std::vector<std::string> Histogram::getXTicks(int num_entries) {  
  int bins_per_entry = (this->num_bins - 1) / (num_entries - 1);
  
  std::vector<std::string> xticks;
  for (int j = 1; j < num_bins; j += bins_per_entry) {
    xticks.push_back(str(boost::format("\"%.2f\" %d") 
        % this->maxvals[j - 1] % j));
  }
  return xticks;
};

void Histogram::plot(int variant) {
  this->plot(variant, "");
};

void Histogram::plot(int variant, std::string lineplotstring) {
  std::vector<double> uncert;
  std::vector<double> plotbins (this->num_bins);
  switch (variant) {
    case 0:
      gp << "plot '-' with boxes title 'Histogram bars'\n";
      gp.send1d(this->bin);
      return;
    case 1:
      uncert = Histogram::frequentistUncertainty();
      for (int j = 0; j < this->num_bins; j++)
        plotbins[j] = (double) this->bin[j];
      break;
    case 2:
      uncert = Histogram::frequentistUncertainty();
      for (int j = 0; j < this->num_bins; j++) {
        plotbins[j] = (double) this->bin[j] / this->total_elements;
        uncert[j] /= this->total_elements;
      }
      break;
    case 3:
      for (int j = 0; j < this->num_bins; j++)
        plotbins[j] = ((double) this->bin[j] + 1) / 
            (this->total_elements + this->num_bins + 1);
      uncert = Histogram::bayesianUncertainty(plotbins);
      break;
    default:
      std::cout << "Unrecognized plotting variant: " << variant << std::endl;
      return;
  }
  this->gp << "set style histogram errorbars gap 0 lw 1\n";
  this->gp << "set style data histogram\n";
  std::vector<std::string> xticks = Histogram::getXTicks(5);
  this->gp << "set xrange [0:" << this->num_bins << "]\n";
  this->gp << "set xtics (";
  for (auto tic : xticks)
    gp << tic << ",";
  this->gp << ")\n";
  if (lineplotstring != "") {
    this->gp << "x = \"(x * " << this->range / this->num_bins << " + " << 
      this->min << ")\"\n";
    this->gp << "plot " << this->bin_width[0] << " * " << lineplotstring << 
      " with lines title 'Theoretical PDF'\n";
    this->gp << "re";
  }
  this->gp << "plot '-' using 1:2 title 'Histogram bars'\n";

  this->gp.send1d(boost::make_tuple(plotbins, uncert));
};

std::vector<double> Histogram::frequentistUncertainty() {
  std::vector<double> uncert (this->num_bins);
  for (int b = 0; b < this->num_bins; b++) {
    uncert[b] = sqrt(this->bin[b] * (1 - this->bin[b] / this->total_elements));
  }
  return uncert;
};

std::vector<double> Histogram::bayesianUncertainty(std::vector<double> avg_pi){
  std::vector<double> uncert (this->num_bins);
  for (int b = 0; b < this->num_bins; b++)
    uncert[b] = sqrt(avg_pi[b] * (1 - avg_pi[b]) / 
        (this->total_elements + this->num_bins + 2));
  return uncert;
};

void Histogram::printBins() {
  std::cout << "Remainder bin elements: " << this->remainder_bin << std::endl;
  std::cout << this->min << " to " << this->maxvals[0] << ": " << this->bin[0]
      << std::endl;
  for (int j = 0; j < this->num_bins - 1; j++)
    std::cout << this->maxvals[j] << " to " << this->maxvals[j + 1] << ": "
        << this->bin[j + 1] << std::endl;
};

void Histogram::_set(std::string key, std::string value,
    std::string delim_left = "\"", std::string delim_right = "\"") {
  this->gp << "set " << key << " " << delim_left << value << delim_right << "\n";
  this->gp << "replot\n";
}

void Histogram::xlabel(std::string label_str) {
  this->_set("xlabel", label_str);
}

void Histogram::ylabel(std::string label_str) {
  this->_set("ylabel", label_str);
}

void Histogram::title(std::string title_str) {
  this->_set("title", title_str);
}
// ########################################################################## //


/*
bool holdon = false;

void hold_on() {
  holdon = true;
}

void hold_off() {
  holdon = false;
}*/

} // end namespace
#endif
