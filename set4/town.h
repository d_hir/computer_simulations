#include <vector>
#include <iostream>
#include <cmath>
#include "rg.h"

RandomGenerator rg;

enum class Metric { class_default=0,
    euclidean=1, standard=1,
    manhattan=2, taxicab=2,
    minkowski=3, spacetime=3
};

class Town {
  private:
    Town(const Town&);
    Town& operator=(const Town&);
    std::vector<double> x;
    int num_dims;
    static Metric metric;
    
  public:
    Town(const int number_dimensions = 2) {
      this->place(number_dimensions);
    };
    
    Town(std::vector<double> coords) {
      this->num_dims = coords.size();
      this->place(coords);
    };
    
    void place(const int number_dimensions = 2) {
      this->num_dims = number_dimensions;
      for (int j = 0; j < number_dimensions; j++)
        x.push_back(rg.rand());    
    };
    
    void place(std::vector<double> pos) {
      this->x = pos;
    };
    
    double getCoord(int dim) const {
      return this->x[dim];
    };
        
    std::vector<double> getCoords() const {
      return this->x;
    };
    
    static double distance(Town* T1, Town* T2, Metric m = Metric::class_default) {
      if (m == Metric::class_default)
        m = Town::metric;
      double dist = 0.0;
      switch (m) {
        case Metric::euclidean: // standard euclidean metric
          for (int j = 0; j < T1->num_dims; j++)
            dist += (T1->x[j] - T2->x[j]) * (T1->x[j] - T2->x[j]);
          dist = std::sqrt(dist);
          break;
        case Metric::manhattan: // manhattan/taxicab metric
          for (int j = 0; j < T1->num_dims; j++)
            dist += std::abs(T1->x[j] - T2->x[j]);
          break;
        case Metric::minkowski: // squared minkowski metric (+---)
          dist += (T1->x[0] - T2->x[0]) * (T1->x[0] - T2->x[0]);
          for (int j = 1; j < T1->num_dims; j++)
            dist -= (T1->x[j] - T2->x[j]) * (T1->x[j] - T2->x[j]);
          break;
        default:
          std::cout << "TOWN: Metric undefined: " << (int) m << std::endl;
          dist = -1.0;
      }
      return dist;
    };
    
    static void setMetric(const Metric m) {
      Town::metric = m;
    };
    
    friend std::ostream& operator<<(std::ostream& os, const Town& T1);
};

Metric Town::metric = Metric::euclidean;

std::ostream& operator<<(std::ostream& os, const Town& T1) {
  os << "Town at (";
  std::vector<double> x = T1.getCoords();
  for (std::vector<double>::iterator it = x.begin(); it < x.end() - 1; it++)
    os << *it << ", ";
  os << *(x.end() - 1) << ")";
  return os;
}
