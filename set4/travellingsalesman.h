#include <vector>
#include "town.h"
#include "plotutils.h"
#include "rg.h"
#include "vectorutils.h"
#include <algorithm>
#include <cmath>
#include <string>
#include <boost/format.hpp>

void swap(int& i, int& j) {
  int k = i; i = j; j = k;
};

struct KVals {
  int k_min;
  int k_max;
  KVals(int k1, int k2) : k_min(k1), k_max(k2) {};
};

class TravellingSalesman {
  private:
    std::vector<Town*> towns;
    int num_towns;
    int num_dims;
    RandomGenerator rg;
    RandomGenerator rg_nozero;
    double beta;
    double beta_start;
    double q;
    double current_energy;
    double total_energy;
    double total_energy_squared;
    double min_energy;
    std::vector<Town*> best_path;
    std::vector<Town*>::iterator begin_it;
    std::vector<double> energy_mean;
    std::vector<double> energy_variance;
    std::vector<double> energy_minimum;
    std::vector<double> beta_vec;
    plotutils::Figure<> intime_fig;
    std::vector<plotutils::Figure<>*> figs_for_deletion;
    int UPDATES_PER_BETA = -1;
  
  public:
    TravellingSalesman(int N, double q_ = 1.0, int num_dims_ = 2) : 
        rg(0, N - 1, 0.0, 1.0), rg_nozero(3, N - 3, 0.0, 1.0) {
      this->num_dims = num_dims_;
      this->num_towns = N;
      if (this->num_towns < 6)
        std::cout << "ERROR: NEED AT LEAST 6 TOWNS" << std::endl;
      this->q = q_;
      this->beta_start = 1.0;
      this->beta = beta_start;
      for (int j = 0; j < this->num_towns; j++)
        this->towns.push_back(new Town(num_dims));
      this->begin_it = this->towns.begin();
      this->best_path.assign(this->towns.begin(), this->towns.end());
      this->plotLandscape("Current Shortest Path: Starting Config", true, true, 
          &this->intime_fig);
    }
    
    TravellingSalesman(std::vector<std::vector<double>> coords, 
        double q_ = 1.0) : num_towns(coords.size()), 
        rg(0, this->num_towns - 1, 0.0, 1.0), 
        rg_nozero(3, this->num_towns - 3, 0.0, 1.0) {
      this->num_dims = coords[0].size();
      if (this->num_towns < 6)
        std::cout << "ERROR: NEED AT LEAST 6 TOWNS" << std::endl;
      this->q = q_;
      this->beta_start = 1.0;
      this->beta = beta_start;
      for (auto& c : coords)
        this->towns.push_back(new Town(c));
      this->begin_it = this->towns.begin();
      this->best_path.assign(this->towns.begin(), this->towns.end());
      this->plotLandscape("Current Shortest Path: Starting Config", true, true, 
          &this->intime_fig);
    }
    
    ~TravellingSalesman() {
      for (auto& t : this->towns)
        delete t;
      for (auto& f : this->figs_for_deletion)
        delete f;
    }
    
    static void setMetric(const Metric m) {
      Town::setMetric(m);
    }
    
    void setUpdatesPerBeta(int updates_per_beta_ = -1) {
      this->UPDATES_PER_BETA = updates_per_beta_;
    }
    
    void printTowns() const {
      for (auto& t : this->towns)
        std::cout << *t << std::endl;
    }
    
    void plotLandscape(std::string title_str = "", bool plot_path = false, 
        bool plot_best_path = false, 
        plotutils::Figure<>* fig = new plotutils::Figure<>(false)) const {
      if (this->num_dims != 2) {
        std::cout << "<Landscape Plot> ABORTED: Can only plot 2d grids!" 
            << std::endl;
        return;
      }
      plotutils::Figure<> f_tmp;
      if (!fig->isValid()) {
        fig = &f_tmp;
      }
      std::vector<double> x, y;
      if (plot_best_path) {
        for (auto& t : this->best_path) {
          x.push_back(t->getCoord(0));
          y.push_back(t->getCoord(1));
        }
        if (plot_path) {
          x.push_back((*(this->best_path.begin()))->getCoord(0));
          y.push_back((*(this->best_path.begin()))->getCoord(1));
        }
      } else {
        for (auto& t : this->towns) {
          x.push_back(t->getCoord(0));
          y.push_back(t->getCoord(1));
        }
        if (plot_path) {
          x.push_back((*(this->towns.begin()))->getCoord(0));
          y.push_back((*(this->towns.begin()))->getCoord(1));
        }
      }
      if (plot_path)
        fig->plot(x, y, "", "", "lines");
      else
        fig->plot(x, y);
      if (title_str != "")
        fig->title(title_str);
    }
    
    void plotInTime() {
      if (this->num_dims != 2)
        return;
      this->intime_fig.overrideWithNextPlot();
      this->plotLandscape(str(boost::format(
          "Current Shortest Path: {/Symbol b} = %1.4f") % this->beta), 
          true, true, &this->intime_fig);
      this->intime_fig.flush();
    }
    
    double getEnergy() const {
      double energy = Town::distance(*this->towns.begin(), 
          *(this->towns.end() - 1));
      for (int j = 0; j < this->num_towns - 1; j++)
        energy += Town::distance(this->towns[j], this->towns[j+1]);
      return energy;
    }
    
    double diffEnergy(const int a, const int b) const {
      int am1 = a - 1;
      if (a == 0)
        am1 = this->num_towns - 1;
      int bp1 = b + 1;
      if (b == this->num_towns - 1)
        bp1 = 0;
      double dE = Town::distance(this->towns[am1], this->towns[b]);
      dE += Town::distance(this->towns[a], this->towns[bp1]);
      dE -= Town::distance(this->towns[am1], this->towns[a]);      
      dE -= Town::distance(this->towns[b], this->towns[bp1]);
      return dE;
    }
    
    double update(const int num_updates = 1) {
      double dE_tot = 0.0;
      double dE, p_accept;
      int a, b;
      for (int j = 0; j < num_updates; j++) {
        a = rg.randn();
        b = (a + rg_nozero.randn()) % this->num_towns;
        if (a > b) swap(a, b);
        dE = this->diffEnergy(a, b);
        p_accept = std::exp(-this->beta * dE);
        if (rg.rand() < p_accept) {
          std::reverse(this->towns.begin() + a, this->towns.begin() + b + 1);
          dE_tot += dE;
        }
      }
      return dE_tot;
    }
    
    void updateSingleTemperature(int num_updates = -1) {
      if (num_updates == -1) num_updates = this->num_towns * this->num_towns;
      this->current_energy = this->getEnergy();
      this->total_energy = 0.0;
      this->total_energy_squared = 0.0;
      this->min_energy = this->current_energy;
      for (int j = 0; j < num_updates; j++) {
        double dE = this->update();
        this->current_energy += dE;
        this->total_energy += this->current_energy;
        this->total_energy_squared += 
            this->current_energy * this->current_energy;
        if (this->current_energy < this->min_energy) {
          this->min_energy = this->current_energy;
          this->best_path.assign(this->towns.begin(), this->towns.end());
        }
      }
      this->energy_mean.push_back(this->total_energy / num_updates);
      this->energy_variance.push_back(this->total_energy_squared / num_updates -
          this->energy_mean.back() * this->energy_mean.back());
      this->energy_minimum.push_back(this->min_energy);
    }
    
    void simulate(std::vector<KVals> k_vec) {
      for (KVals KVal : k_vec) {
        for (int k = KVal.k_min; k <= KVal.k_max; k++) {
          this->beta = this->beta_start * std::pow(k, this->q);
          this->beta_vec.push_back(beta);
          updateSingleTemperature(this->UPDATES_PER_BETA);
          this->plotInTime();
          this->towns.assign(this->best_path.begin(), this->best_path.end());
        }
      }
      plotutils::Figure<> mean_energy_fig;
      mean_energy_fig.plot(this->beta_vec, this->energy_mean);
      mean_energy_fig.title("Mean of energy for each temperature");
      mean_energy_fig.ylabel("mean(E)");
      mean_energy_fig.xlabel("{/Symbol b}");
      plotutils::Figure<> variance_energy_fig;
      variance_energy_fig.plot(this->beta_vec, this->energy_variance);
      variance_energy_fig.title("Variance of energy for each temperature");
      variance_energy_fig.ylabel("var(E)");
      variance_energy_fig.xlabel("{/Symbol b}");
      plotutils::Figure<> min_energy_fig;
      min_energy_fig.plot(this->beta_vec, this->energy_minimum);
      min_energy_fig.title("Minimum of energy for each temperature");
      min_energy_fig.ylabel("min(E)");
      min_energy_fig.xlabel("{/Symbol b}");
      std::cout << "Shortest found path length: " << getEnergy() << std::endl;
      plotutils::Figure<> beta_fig;
      beta_fig.plot(this->beta_vec);
      beta_fig.title("{/Symbol b} for each simulation");
      beta_fig.xlabel("iteration");
      beta_fig.ylabel("{/Symbol b}");
    }
};
