#include <iostream>
#include <vector>
#include "timer.h"
#include "rg.h"

#define N_NUM 10
#define N_MIN 10000
#define N_INC 10000
#define RUNS 100

template<typename T>
std::ostream& operator<<(std::ostream& stream, const std::vector<T>& print_vector) {
  stream.put('[');
  char delimiter[3] = {'\0', ' ', '\0'};
  for (const auto& element : print_vector) {
    stream << delimiter << element;
    delimiter[0] = ',';    
  }
  stream << ']';
  return stream;
}

template<typename T>
void randomize(std::vector<T>& vec, double min, double max) {
  RandomGenerator generator(0, 1, min, max);
  for (auto& element : vec)
    element = generator.rand();
}

template<typename T>
T mean(std::vector<T>& vec) {
  T meanval(vec[0]);
  int len = static_cast<int>(vec.size());
  for (int k = 1; k < len; k++)
    meanval = meanval + vec[k];
  return meanval / len;
}

int main() {
  for (int N = N_MIN; N < N_MIN + N_NUM * N_INC; N += N_INC) {
    std::vector<double> Btime(RUNS);
    std::vector<double> Ctime(RUNS);
    for (int k = 0; k < RUNS; k++) {
      Timer timerB;
      Timer timerC;
      
      //int N = 100;
      std::vector<double> x(N);
      //std::cout << x << std::endl;
      randomize(x, 0.0, 1.0);  
      //std::cout << x << std::endl;
      
      std::vector<double> B(N);
      timerB.start();
      B[N - 1] = x[0];
      for (int j = N - 2; j >= 0; j--)
        B[j] = B[j + 1] + x[N - 1 - j];
      timerB.end();
      //std::cout << B << std::endl;
      Btime[k] = timerB.get();
      
      std::vector<double> C(N);
      timerC.start();
      C[N - 1] = x[N - 1];
      for (int j = N - 2; j >= 0; j--)
        C[j] = C[j + 1] + x[j];
      timerC.end();
      Ctime[k] = timerC.get();
      //std::cout << C << std::endl;
    }
    
    
    std::cout << " B took " << Timer(mean(Btime)) << "   |   ";
    std::cout << " C took " << Timer(mean(Ctime)) << std::endl;
  }
  return 0;
}
