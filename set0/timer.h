#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <iostream>

class Timer {
  private:
    std::chrono::steady_clock::time_point startpoint;
    std::chrono::steady_clock::time_point endpoint;
    double timedelta = 0;
    
  public:
    Timer(){};
    Timer(double timedelta_);
    void start();
    void end();
    double get() const;
    
    friend std::ostream& operator<<(std::ostream& stream, const Timer& timer);
};



#endif
